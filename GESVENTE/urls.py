"""GESVENTE URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from gesvente.views import acceuil,connexion,user,categorie,ajout_payement,deconexion,delete_utilisateur,client,payement,update_utilisateur,delete_article,delete_client,commande,article,delete_categorie,update_client,ajout_utilisateur,ajout_categorie,ajout_article,ajout_client,ajout_commande,update_categorie,update_article

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", connexion , name="connexion"),
    path('acceuil',acceuil,name='acceuil'),
    path("user", user, name="utilisateur"),
    path("categorie", categorie, name="categorie"),
    path("client", client, name="client"),
    path("commande", commande, name="commande"),
    path("article", article, name="article"),
    path("payement", payement, name="payement"),
    path("ajout_utilisateur", ajout_utilisateur, name="ajout_utilisateur"),
    path("ajout_categorie", ajout_categorie, name="ajout_categorie"),
    path("ajout_commande", ajout_commande, name="ajout_commande"),
    path("ajout_client", ajout_client, name="ajout_client"), 
    path("ajout_article", ajout_article, name="ajout_article"),    
    path("update_categorie/<int:id>", update_categorie, name="update_cat"),
    path("update_article/<int:id>", update_article, name="update_article"),    
    path("update_client/<int:id>", update_client, name="update_client"), 
    path("update_utilisateur/<int:id>", update_utilisateur, name="update_utilisateur"), 
    path("delete_categorie/<int:id>", delete_categorie, name="delete_cat"),
    path("delete_client/<int:id>", delete_client, name="delete_cli"), 
    path("delete_article/<int:id>", delete_article, name="delete_art"),
    path("delete_utilisateur/<int:id>", delete_utilisateur, name="delete_utilisateur"),  
    path("logout", deconexion, name="logout"),
    
    
]




if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
    document_root = settings.MEDIA_ROOT)
