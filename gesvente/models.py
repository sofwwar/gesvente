from django.db import models
from django.urls import reverse
from django.contrib.auth.models import AbstractUser
# Create your models here.

# Classe Client
class Client(models.Model):
    nom = models.CharField( max_length=250)
    prenom = models.CharField( max_length=250)
    email = models.EmailField( max_length=254,unique=True)
    tel = models.CharField(max_length=250)
    

    class Meta:
        verbose_name = ("Client")
        verbose_name_plural = ("Clients")

    def __str__(self):
        return self.nom

    def get_absolute_url(self):
        return reverse("Client_detail", kwargs={"pk": self.pk})

#Class Catégorie

class Categorie(models.Model):

    nom_categorie = models.CharField(max_length=150)

    class Meta:
        verbose_name = ("Categorie")
        verbose_name_plural = ("Categories")

    def __str__(self):
        return self.nom_categorie

    def get_absolute_url(self):
        return reverse("Categorie_detail", kwargs={"pk": self.pk})

# Class Article

class Article(models.Model):
    nom_article = models.CharField(max_length=250)
    description = models.CharField(max_length=250)
    prix = models.IntegerField()
    quantite = models.IntegerField()
    id_cat = models.ForeignKey("Categorie", on_delete=models.CASCADE)
    

    class Meta:
        verbose_name = ("Article")
        verbose_name_plural = ("Articles")

    def __str__(self):
        return self.nom_article

    def get_absolute_url(self):
        return reverse("Article_detail", kwargs={"pk": self.pk})
    
# Class Utilisateur

class User(AbstractUser):
    CREATOR = 'CREATOR'
    SUBSCRIBER = 'SUBSCRIBER'

    ROLE_CHOICES = (
    (CREATOR, 'Créateur'),
    (SUBSCRIBER, 'Abonné'),
    )
    statut_user = models.CharField(max_length=250)

class Commande(models.Model):
    article = models.CharField(max_length=250)
    quantiter = models.IntegerField()
    prix = models.IntegerField()
    prix_total = models.IntegerField()
    date = models.DateField(auto_now=False)
    client = models.CharField(max_length=250)
    
    class Meta:
        verbose_name = ("Commande")
        verbose_name_plural = ("Commandes")
    
    def __str__(self):
        return self.date
    
    def get_absolute_url(self):
        return reverse("Commande_detail", kwargs={"pk": self.pk})

class Payemment(models.Model):
    moyen = models.CharField(max_length=250)
    montant = models.IntegerField()
    date_transaction = models.DateField(auto_now=False)
    id_cmd = models.ForeignKey("Commande",on_delete=models.CASCADE)

    

    class Meta:
        verbose_name = ("Payement")
        verbose_name_plural = ("Payements")

    def __str__(self):
        return self.moyen

    def get_absolute_url(self):
        return reverse("Payement_detail", kwargs={"pk": self.pk})
