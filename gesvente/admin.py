from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Client,Article,Commande,Payemment,User,Categorie

# Register your models here.
class ArticleAdmin(admin.ModelAdmin):
    list_display=('nom_article','description','prix','quantite','id_cat')
admin.site.register(Article,ArticleAdmin)

class ClientAdmin(admin.ModelAdmin):
    list_display=('nom','prenom','email','tel')
admin.site.register(Client,ClientAdmin)

class CommandeAdmin(admin.ModelAdmin):
    list_display=('client','article','quantiter','prix','prix_total','date')
admin.site.register(Commande,CommandeAdmin)

class CategorieAdmin(admin.ModelAdmin):
    list_display=['nom_categorie']
admin.site.register(Categorie,CategorieAdmin)


class PayemmentAdmin(admin.ModelAdmin):
    list_display=['moyen','montant','date_transaction']
admin.site.register(Payemment,PayemmentAdmin)

class UserAdmin(admin.ModelAdmin):
    list_display=['first_name','last_name','email','is_active','statut_user']
admin.site.register(User,UserAdmin)
