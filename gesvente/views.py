from pydoc import cli
from django.shortcuts import render, get_object_or_404
from .models import Categorie,Article,Payemment,User,Commande,Client
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout 
from django.http import JsonResponse

# Create your views here.



#fonction acceuil

def acceuil(request):
    return render(request,'acceuil.html')

# Fonction qui recupère la connexion

def connexion(request):
    if request.method == 'POST':
        nom = request.POST.get('nom')
        mdp = request.POST.get('mdp')
        user = authenticate(username = nom ,password = mdp)
        if user is not None:
            if user.statut_user == 'admin':
                login(request,user)
                request.session["username"]=nom
                return redirect('utilisateur')
            else:
                login(request,user)
                request.session["username"]=nom
                return redirect('acceuil')
        else:
            messages.error(request,"Mot de passe ou Identifiant erroné")
            return redirect('connexion')
    return render(request,'login.html')

#--------------------------------------------------------------------|
# Traitement des données: Selection des données de la base de donnée |
#--------------------------------------------------------------------|

# Fonction qui recupère la liste des utilisateurs

def user(request):
    liste_user = User.objects.all()
    return render(request,'utilisateur.html',{'list_user':liste_user})

# Fonction qui recupère la liste des categories

def categorie(request):
    liste_categorie = Categorie.objects.all()
    return render(request,'categorie.html',{'liste_cat':liste_categorie})

# Fonction qui recupère la liste des articles

def article(request):
    liste_article = Article.objects.all()
    return render(request,'Article.html',{'liste_art':liste_article})

# Fonction qui recupère la liste des commandes

def commande(request):
    return render(request,'commande.html')

# Fonction qui recupère la liste des clients

def client(request):
    liste_client = Client.objects.all()
    return render(request,'Client.html',{'liste_cli':liste_client})

# Fonction qui recupère la liste des payement

def payement(request):
    return render(request,'payement.html')

#--------------------------------------------------------------------|
# Traitement des données: Ajout des données dans la base de donnée   |
#--------------------------------------------------------------------|

# Fonction qui gère l'ajout des utilisateur

def ajout_utilisateur(request):
    if request.method == 'POST':
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        email = request.POST['email']
        mdp = request.POST['mdp']
        statut = request.POST['statut']
        user = User.objects.create_superuser(username = nom,last_name = prenom,email = email,password = mdp,statut_user=statut)
        user.save()
        return redirect('utilisateur')
    return render(request,'ajout_utilisateur.html')

# Fonction qui gère l'ajout des catégorie

def ajout_categorie(request):
    if request.method == 'POST':
        nom_cat = request.POST['nom_cat']
        categorie = Categorie.objects.create(nom_categorie = nom_cat)
        categorie.save()
        messages.success(request,'La catégorie à été ajouter avec succès')
        return HttpResponseRedirect('/categorie')

    else:
        messages.error(request,"La catégorie n'as pas été ajouter" )

    return render(request,'ajout_cartegorie.html')

#

def ajout_client(request):
    if request.method == 'POST':
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        email = request.POST['email']
        tel = request.POST['tel']
        client = Client.objects.create(nom = nom,prenom = prenom,email = email,tel = tel)
        client.save()
        return redirect('client')
    return render(request,'ajout_client.html')

# Fonction qui ajoute les commande

def ajout_commande(request):
   produit = Article.objects.all()
   client = Client.objects.all()
   if request.method == 'POST':
        date = request.POST['date']
        client = request.POST['client']
        produit = request.POST['produit']
        quantiter = request.POST['quantiter']
        prix = request.POST['prix']
        prix_totale = int(request.POST['quantiter']) * int(request.POST['prix'])
        commande = Commande.objects.create(article = produit,quantiter = quantiter,prix = prix,prix_total = prix_totale,date = date,client = client)
        commande.save()
   return render(request,'ajout_commande.html',{'produits':produit,'clients':client})
   
    
# Fonction qui ajoute les payement

def ajout_payement(request):
    return render(request,'ajout_commande.html')

# Founction qui ajoute les articles 

def ajout_article(request):
    
    if request.method == 'POST':
        nom = request.POST["nom"]
        description = request.POST['description']
        prix = request.POST['prix']
        quantite = request.POST['quantite']
        id_cate =request.POST['categorie']
        id_cat = Categorie.objects.get(id = id_cate)
        article = Article.objects.create(nom_article = nom,description = description,prix = prix,quantite = quantite,id_cat = id_cat)
        article.save()
        return redirect('article')
    else:
        cat =Categorie.objects.all()
        return render(request,'ajout_article.html',{'categorie':cat,})
    
#--------------------------------------------------------------------|
# Traitement des données: Mise des données dans la base de donnée    |
#--------------------------------------------------------------------|

# Mise à jour des données catégrie

def update_categorie(request,id):
    categorie = get_object_or_404(Categorie, pk = id)
    if request.method == 'POST':
        print(request.POST.get('nom_categorie'))
        categorie.nom_categorie = request.POST.get('nom_cat')
        categorie.save()
        return redirect('categorie')
    return render(request,'ajout_cartegorie.html',{'cat':categorie})

def update_article(request,id):
    article = get_object_or_404(Article, pk = id)
    if request.method == 'POST':
        article.nom_article = request.POST.get('nom')
        article.description = request.POST.get('description')
        article.prix = request.POST.get('prix')
        article.quantite = request.POST.get('quantite')
        id_cat = Categorie.objects.get(nom_categorie = request.POST.get('id_cat'))
        article.id_cat = id_cat

        article.save()
        return redirect('article')
    return render(request,'update_article.html',{'article':article})


def update_client(request,id):
    client = get_object_or_404(Client, pk = id)
    if request.method == 'POST':
        client.nom = request.POST.get('nom')
        client.prenom = request.POST.get('prenom')
        client.email = request.POST.get('email')
        client.tel = request.POST.get('tel')
        client.save()
        return redirect('client')
    return render(request,'ajout_client.html',{'client':client})

def update_utilisateur(request,id):
    utilisateur = get_object_or_404(User, pk = id)
    if request.method == 'POST':
        utilisateur.username = request.POST.get('nom')
        utilisateur.last_name = request.POST.get('prenom')
        utilisateur.email = request.POST.get('email')
        utilisateur.password = request.POST.get('mdp')
        utilisateur.statut_user = request.POST.get('statut')
        utilisateur.save()
        return redirect('utilisateur')
    return render(request,'ajout_utilisateur.html',{'users':utilisateur})

#----------------------------------------------------------------------|
# Traitement des données: supression des données dans la base de donnée|
#----------------------------------------------------------------------|

def delete_categorie(request,id):
    categorie = Categorie.objects.get(id = id)
    categorie.delete()
    return HttpResponseRedirect('/categorie')

def delete_client(request,id):
    client = Client.objects.get(id = id)
    client.delete()
    return HttpResponseRedirect('/client')

def delete_article(request,id):
    article = Article.objects.get(id = id)
    article.delete()
    return HttpResponseRedirect('/article')

def delete_utilisateur(request,id):
    utilisateur = User.objects.get(id = id)
    utilisateur.delete()
    return redirect('utilisateur')

def deconexion(request):
    try:
      del request.session['username']
    except:
      pass
    return redirect("connexion")